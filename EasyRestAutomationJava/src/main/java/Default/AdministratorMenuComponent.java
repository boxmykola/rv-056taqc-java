package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AdministratorMenuComponent extends UserMenuCommonComponent{

    private static final String ADMINISTRATOR_PANEL_BUTTON = "//a[text()='Administrator panel']";

    @FindBy(how = How.XPATH, using = ADMINISTRATOR_PANEL_BUTTON)
    private WebElement administratorPanelButton;

    public AdministratorMenuComponent(WebDriver driver) {
        super(driver);
    }

    public void clickOnAdministratorPanelButton() {
        administratorPanelButton.click();
    }

}

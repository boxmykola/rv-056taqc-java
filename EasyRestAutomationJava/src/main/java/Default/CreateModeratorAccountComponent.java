package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CreateModeratorAccountComponent {

    private static final String NAME_FIELD = "//*[@id=\"root\"]/div/main/div/div/form/div/div/div[1]/div/div/input";

    private static final String EMAIL_FIELD = "//*[@id=\"root\"]/div/main/div/div/form/div/div/div[2]/div/div/input";

    private static final String PHONE_NUMBER_FIELD = "//*[@id=\"root\"]/div/main/div/div/form/div/div/div[3]/div/div/input";

    private static final String BIRTH_DATE_FIELD = "//*[@id=\"root\"]/div/main/div/div/form/div/div/div[4]/div/div[1]/div/div/input";

    private static final String PASSWORD_FIELD = "//*[@id=\"root\"]/div/main/div/div/form/div/div/div[5]/div/div/input";

    private static final String CONFIRM_PASSWORD_FIELD = "//*[@id=\"root\"]/div/main/div/div/form/div/div/div[6]/div/div/input";

    private static final String CREATE_ACCOUNT_BUTTON = "//*[@id=\"root\"]/div/main/div/div/form/div/div/div[7]/div/button/span[1]";

    private static final String CANCEL_BUTTON = "//*[@id=\"root\"]/div/main/div/div/form/div/div/div[7]/div/a/span[1]";

    @FindBy(how = How.XPATH, using = CREATE_ACCOUNT_BUTTON)
    private WebElement createAccountButton;

    @FindBy(how = How.XPATH, using = CANCEL_BUTTON)
    private WebElement cancelButton;

    private WebDriver driver;

    public CreateModeratorAccountComponent(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void enterName(String name) {
        driver.findElement(By.id(NAME_FIELD)).sendKeys(name);
    }

    public void enterEmail(String email) {
        driver.findElement(By.id(EMAIL_FIELD)).sendKeys(email);
    }

    public void enterPhoneNumber(String phoneNumber) {
        driver.findElement(By.id(PHONE_NUMBER_FIELD)).sendKeys(phoneNumber);
    }

    public void enterBirthDate(String birthDate) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement birthDateElement = driver.findElement(By.id(BIRTH_DATE_FIELD));
        String setValueAttributeJavaScript = "arguments[0].setAttribute('value', '" + birthDate + "')";
        js.executeScript(setValueAttributeJavaScript, birthDateElement);
    }

    public void enterPassword(String password) {
        driver.findElement(By.id(PASSWORD_FIELD)).sendKeys(password);
    }

    public void enterConfirmPassword(String confirmPassword) {
        driver.findElement(By.id(CONFIRM_PASSWORD_FIELD)).sendKeys(confirmPassword);
    }

    public void addModeratorAccount(String name, String email, String phoneNumber,
            String birthDate, String password, String confirmPassword) {
        enterName(name);
        enterEmail(email);
        enterPhoneNumber(phoneNumber);
        enterBirthDate(birthDate);
        enterPassword(password);
        enterConfirmPassword(confirmPassword);
        createAccountButton.click();
    }

    public void clickOnCreateAccountButton() {
        createAccountButton.click();
    }

    public void clickOnCancelButton() {
        cancelButton.click();
    }
}


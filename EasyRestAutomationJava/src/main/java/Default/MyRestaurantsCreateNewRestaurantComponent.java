package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyRestaurantsCreateNewRestaurantComponent extends CommonPageObject{

    private static final String RESTAURANTS_NAME = "//*[@id=\"root\"]/main/div/div/div/div[1]/div[3]/div/div/div/div[2]/form/div/div[1]/div/div/input";
    private static final String RESTAURANT_ADDRESS = "//*[@id=\"root\"]/main/div/div/div/div[1]/div[3]/div/div/div/div[2]/form/div/div[2]/div/div/input";
    private static final String RESTAURANT_PHONE = "//*[@id=\"root\"]/main/div/div/div/div[1]/div[3]/div/div/div/div[2]/form/div/div[3]/div/div/input";
    private static final String RESTAURANT_PREVIEW_TEXT = "//*[@id=\"root\"]/main/div/div/div/div[1]/div[3]/div/div/div/div[2]/form/div/div[4]/div/div/textarea";
    private static final String RESTAURANT_DESCRIPTION = "//*[@id=\"root\"]/main/div/div/div/div[1]/div[3]/div/div/div/div[2]/form/div/div[7]/div/div[2]/div/div/div/div/div/h2/div";
    private static final String CANCEL_BUTTON = "//span[text()=\"Cancel\"]";
    private static final String HEADING_BUTTON = "//button[@title=\"Heading\"]";
    private static final String BLOCKQUOTE_BUTTON = "//button[@title=\"Blockquote\"]";
    private static final String UL_BUTTON = "//button[@title=\"UL\"]" ;
    private static final String OL_BUTTON = "//button[@title=\"OL\"]" ;
    private static final String BOLD_BUTTON = "//button[@title=\"Bold\"]";
    private static final String ITALIC_BUTTON = "//button[@title=\"Italic\"]";
    private static final String UNDER_LINE_BUTTON = "//button[@title=\"Underline\"]";
    private static final String TAGS_MENU_BUTTON = "//*[@id=\"select-tags\"]";

    private WebElement webElement;

    @FindBy(xpath = TAGS_MENU_BUTTON)
    protected WebElement tagsMenuButton;

    @FindBy(xpath = RESTAURANTS_NAME)
    protected WebElement restaurantName;

    @FindBy(xpath = RESTAURANT_ADDRESS)
    protected WebElement restaurantAddress;

    @FindBy(xpath = RESTAURANT_PHONE)
    protected WebElement restaurantPhone;

    @FindBy(xpath = RESTAURANT_PREVIEW_TEXT )
    protected WebElement restaurantPreviewText;

    @FindBy(xpath = RESTAURANT_DESCRIPTION)
    protected WebElement restaurantDescription;

    @FindBy(xpath = CANCEL_BUTTON)
    protected WebElement cancelButton;

    @FindBy(xpath = HEADING_BUTTON)
    protected WebElement headingButton;

    @FindBy(xpath = BLOCKQUOTE_BUTTON)
    protected WebElement blockquoteButton;

    @FindBy(xpath = UL_BUTTON )
    protected WebElement ulButton;

    @FindBy(xpath = OL_BUTTON )
    protected WebElement olButton;

    @FindBy(xpath = BOLD_BUTTON )
    protected WebElement boldButton;

    @FindBy(xpath = ITALIC_BUTTON )
    protected WebElement italicButton;

    @FindBy(xpath = UNDER_LINE_BUTTON )
    protected WebElement underlineButton;

    public MyRestaurantsCreateNewRestaurantComponent(WebDriver driver, WebElement element) {
        super(driver);
        this.webElement = element;
    }

    public MyRestaurantsCreateNewRestaurantListTags getRestaurantsNewRestaurantList(){
        return new MyRestaurantsCreateNewRestaurantListTags(driver, tagsMenuButton);
    }

    public void clickTagsMenuButton() {
        tagsMenuButton.click();
    }

    public void inputRestaurantName(String Name) {
        restaurantName.sendKeys(Name);
    }

    public void inputRestaurantAddress(String Address) {
        restaurantAddress.sendKeys(Address);
    }

    public void inputRestaurantPhone(String Phone) {
        restaurantPhone.sendKeys(Phone);
    }

    public void inputRestaurantPreviewText(String PreviewText) {
        restaurantPreviewText.sendKeys(PreviewText);
    }

    public void inputRestaurantDescription(String RestaurantDescription) {
        restaurantDescription.sendKeys(RestaurantDescription);
    }

    public void clickOnCancelButton() {
        cancelButton.click();
    }

    public void clickOnHeadingButton() {
        headingButton.click();
    }

    public void clickOnBlockquoteButton() {
        blockquoteButton.click();
    }

    public void clickOnUlButton() {
        ulButton.click();
    }

    public void clickOnOlButton() {
        olButton.click();
    }

    public void clickOnBoldButton() {
        boldButton.click();
    }

    public void clickOnItalicButton() {
        italicButton.click();
    }

    public void clickOnUnderlineButton() {
        underlineButton.click();
    }
}
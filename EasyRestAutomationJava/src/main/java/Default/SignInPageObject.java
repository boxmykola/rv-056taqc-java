package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignInPageObject extends CommonPageObject {
    private static final String EMAIL_FIELD = "//input[@name=\"email\"]";
    private static final String PASSWORD_FIELD = "//input[@name=\"password\"]";
    private static final String SIGN_IN_BUTTON = "//button[@type=\"submit\"]";
    private static final String ERROR_EMAIL_IS_REQUIRED = "//p[contains(text(), \"Email is required\")]";
    private static final String ERROR_PASSWORD_IS_REQUIRED = "//p[contains(text(), \"Password is required\")]";
    private final int timeOutInSeconds = 10;
    private static WebDriverWait wait;

    @FindBy(xpath = EMAIL_FIELD)
    private WebElement emailField;

    @FindBy(xpath = PASSWORD_FIELD)
    private WebElement passwordField;

    @FindBy(xpath = SIGN_IN_BUTTON)
    private WebElement signInButton;

    @FindBy(xpath = ERROR_EMAIL_IS_REQUIRED)
    private WebElement errorEmailIsRequired;

    @FindBy(xpath = ERROR_PASSWORD_IS_REQUIRED)
    private WebElement errorPasswordIsRequired;

    public SignInPageObject(WebDriver driver) {
        super(driver);
        wait = new WebDriverWait(driver, timeOutInSeconds);
    }

    public void enterEmail(String email) {
        emailField.sendKeys(email);
    }

    public void enterPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void clickOnButtonSignIn() {
        signInButton.click();
    }

    public boolean isVisibleErrorEmailIsRequired() {
        wait.until(ExpectedConditions.visibilityOf(errorEmailIsRequired));
        return errorEmailIsRequired.isDisplayed();
    }

    public boolean isVisibleErrorPasswordIsRequired() {
        wait.until(ExpectedConditions.visibilityOf(errorPasswordIsRequired));
        return errorPasswordIsRequired.isDisplayed();
    }
}
package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

    public class OrderWaitingToConfirmExpandedComponent {

    private static final String ACCEPT_BUTTON = "//span[contains(text(), 'Accept')]";

    private WebElement webElement;

    private WebElement acceptButton;

    public OrderWaitingToConfirmExpandedComponent(WebElement webElement) {
        this.webElement = webElement;
        acceptButton = webElement.findElement(By.xpath(ACCEPT_BUTTON));
    }

    public void pressAcceptButton() {
        this.acceptButton.click();
    }
}
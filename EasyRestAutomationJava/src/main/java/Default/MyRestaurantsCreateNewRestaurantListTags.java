package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class MyRestaurantsCreateNewRestaurantListTags extends CommonPageObject {

    private static final String TAGS_MENU = "//*[@id=\"menu-tags\"]/div[2]/ul";

    private WebElement webElement;

    public MyRestaurantsCreateNewRestaurantListTags(WebDriver driver, WebElement element) {
        super(driver);
        this.webElement = element;
    }

    public ArrayList<MyRestaurantsTagsComponent> getTagsComponent() {

        ArrayList<MyRestaurantsTagsComponent> list = new ArrayList<>();
        List<WebElement> tagsList = webElement.findElements(By.xpath(TAGS_MENU));

        for (WebElement element : tagsList) {
            list.add(new MyRestaurantsTagsComponent(element));
        }

        return list;
    }
}
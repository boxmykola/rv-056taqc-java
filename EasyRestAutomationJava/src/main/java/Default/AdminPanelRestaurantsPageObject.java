package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AdminPanelRestaurantsPageObject extends AdminPanelCommonPageObject {

    /*locator*/
    private static final String RESTAURANTS_TABLE_LOCATOR = "//*[@id=\"root\"]/div/main/div[2]/table";

    protected RestaurantsHeaderMenuComponent restaurantsHeaderMenuComponent;

    public AdminPanelRestaurantsPageObject(WebDriver driver) {
        super(driver);
        this.restaurantsHeaderMenuComponent = new RestaurantsHeaderMenuComponent(driver);
    }

    public RestaurantsHeaderMenuComponent getRestaurantsHeaderMenuComponent() {
        return restaurantsHeaderMenuComponent;
    }

    public AdminPanelRestaurantsTableComponent getRestaurants() {
        WebElement webElement = driver.findElement(By.xpath(RESTAURANTS_TABLE_LOCATOR));
        return new AdminPanelRestaurantsTableComponent(webElement);
    }
}


package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.List;

public class MyRestaurantsMoreButtonListComponent extends CommonPageObject {
    private WebElement webElement;

    private static final String MORE_LIST = "//*[@id=\"item-menu\"]/div[2]";

    public MyRestaurantsMoreButtonListComponent(WebDriver driver, WebElement element) {
        super(driver);
        this.webElement = element;
    }

    public ArrayList<MyRestaurantsMoreButtonComponent> getMoreList() {

        ArrayList<MyRestaurantsMoreButtonComponent> list = new ArrayList<>();
        List<WebElement> moreList = webElement.findElements(By.xpath(MORE_LIST));

        for (WebElement element : moreList) {
            list.add(new MyRestaurantsMoreButtonComponent(element));
        }

        return list;
    }
}
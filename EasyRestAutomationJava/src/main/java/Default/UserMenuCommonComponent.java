package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class UserMenuCommonComponent extends CommonPageObject{

    private static final String LOG_OUT_BUTTON = "//li[text()='Log Out']";

    @FindBy(how = How.XPATH, using = LOG_OUT_BUTTON)
    private WebElement logOutButton;

    public UserMenuCommonComponent(WebDriver driver) {
        super(driver);
    }

    public void clickOnLogOutButton() {
        logOutButton.click();
    }

}

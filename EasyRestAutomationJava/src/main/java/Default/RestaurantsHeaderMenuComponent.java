package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class RestaurantsHeaderMenuComponent extends AdminPanelCommonPageObject {

    /* Locators of page components. */
    private static final String ALL_BUTTON = "//*[@id=\"root\"]/div/main/div[1]/header/div/div[2]/div[2]/div/button[1]/span[1]/span/span";

    private static final String UNAPPROVED_BUTTON = "//*[@id=\"root\"]/div/main/div[1]/header/div/div[2]/div[2]/div/button[2]/span[1]/span/span";

    private static final String APPROVED_BUTTON = "//*[@id=\"root\"]/div/main/div[1]/header/div/div[2]/div[2]/div/button[3]/span[1]/span/span";

    private static final String ARCHIVED_BUTTON = "//*[@id=\"root\"]/div/main/div[1]/header/div/div[2]/div[2]/div/button[4]/span[1]/span/span";

    @FindBy(how = How.XPATH, using = ALL_BUTTON)
    private WebElement allButton;

    @FindBy(how = How.XPATH, using = UNAPPROVED_BUTTON)
    private WebElement unapprovedButton;

    @FindBy(how = How.XPATH, using = APPROVED_BUTTON)
    private WebElement approvedButton;

    @FindBy(how = How.XPATH, using = ARCHIVED_BUTTON)
    private WebElement archivedButton;

    public RestaurantsHeaderMenuComponent(WebDriver driver) {
        super(driver);
    }

    public void clickOnAllButton() {
        allButton.click();
    }

    public void clickOnUnapprovedButton() {
        unapprovedButton.click();
    }

    public void clickOnApprovedButton() {
        approvedButton.click();
    }

    public void clickOnArchivedButton() {
        archivedButton.click();
    }
}


package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.List;

public class AdminPanelCommonOwnersModeratorsTableComponent {

    private WebElement element;

    public AdminPanelCommonOwnersModeratorsTableComponent(WebElement element) {
        this.element = element;
    }

    public List<ModeratorsOwnersRowComponent> getRows() {
        String ROW_TAG_NAME = "tr";
        List<WebElement> rows = element.findElements(By.tagName(ROW_TAG_NAME));
        List<ModeratorsOwnersRowComponent> components = new ArrayList<>();
        for (WebElement row : rows) {
            components.add(new ModeratorsOwnersRowComponent(row));
        }
        return components;
    }
}

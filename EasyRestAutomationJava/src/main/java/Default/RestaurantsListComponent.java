package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.*;

public class RestaurantsListComponent extends CommonPageObject {
    private WebElement webElement;

    public RestaurantsListComponent(WebDriver driver, WebElement element) {
        super(driver);
        this.webElement = element;
    }

    public ArrayList<RestaurantComponent> getRestaurantsList() {

        ArrayList<RestaurantComponent> list = new ArrayList<>();
        List<WebElement> restaurantsList = webElement.findElements(By.xpath("//*[contains(@class, \"TagsTab-item\")]"));

        for (WebElement element : restaurantsList) {
            list.add(new RestaurantComponent(element));
        }

        return list;
    }
}

package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class OrderWaitingToConfirmCollapsedComponent {

    private static final String EXPAND_BUTTON = "//span[@class='MuiIconButton-label-267']";
    private static final String ORDER_ID = "//p[text()='Order id: #']";

    private WebElement webElement;

    private WebElement expandButton;

    private WebElement orderID;

    public OrderWaitingToConfirmCollapsedComponent(WebElement webElement) {
	this.webElement = webElement;
	expandButton = webElement.findElement(By.xpath(EXPAND_BUTTON));
	orderID = webElement.findElement(By.xpath(ORDER_ID));
    }

    public OrderWaitingToConfirmExpandedComponent clickExpandButtonWaitingToConfirm() {
	this.expandButton.click();
	return new OrderWaitingToConfirmExpandedComponent(webElement);
    }
}
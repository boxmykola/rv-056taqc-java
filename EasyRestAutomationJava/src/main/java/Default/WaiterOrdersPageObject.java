package Default;

import org.openqa.selenium.WebDriver;

public class WaiterOrdersPageObject extends WaiterCommonOrdersPageObject {

    public WaiterOrdersPageObject(WebDriver driver) {
        super(driver);
    }

    public WaiterOrdersComponent getWaiterOrders() {
        return new WaiterOrdersComponent(driver,waiterOrdersContainer);
    }
}

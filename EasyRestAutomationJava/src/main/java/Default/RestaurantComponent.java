package Default;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class RestaurantComponent {

    private static final String DETAILS_BUTTON = "//a/span[text()=\"details\"]";
    private static final String WATCH_MENU_BUTTON = "//a/span[text()=\"Watch Menu\"]";
    private WebElement webElement;

    @FindBy(xpath = DETAILS_BUTTON)
    private WebElement detailsButton;

    @FindBy(xpath = WATCH_MENU_BUTTON)
    private WebElement watchMenuButton;

    public RestaurantComponent(WebElement element) {
        this.webElement = element;
        DefaultElementLocatorFactory elementLocatorFactory = new DefaultElementLocatorFactory(webElement);
        PageFactory.initElements(elementLocatorFactory, this);
    }

    public void clickOnDetailsButton() {
        detailsButton.click();
    }

    public void clickOnWatchMenuButton() {
        watchMenuButton.click();
    }
}

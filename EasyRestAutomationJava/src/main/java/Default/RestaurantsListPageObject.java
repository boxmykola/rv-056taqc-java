package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RestaurantsListPageObject extends CommonPageObject{

    private static final String LIST_CATEGORIES = "//div[@role='tablist']/..";
    private static final String LIST_RESTAURANTS = "//div[contains(@class, 'TagsTab-item-')]/..";

    @FindBy(xpath = LIST_CATEGORIES)
    private WebElement blockOfCategories;

    @FindBy(xpath = LIST_RESTAURANTS)
    private WebElement blockOfRestaurants;

    public RestaurantsListPageObject(WebDriver driver) {
        super(driver);
    }

    public RestaurantsListComponent getRestaurants() {
        return new RestaurantsListComponent(driver, blockOfRestaurants);
    }

    public RestaurantsCategoriesComponent getRestaurantCategories() {
        return new RestaurantsCategoriesComponent(driver, blockOfCategories);
    }
}

package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignUpPageObject extends CommonPageObject{
    private static final String USER_NAME_FIELD = "//input[@name=\"name\"]";
    private static final String EMAIL_FIELD = "//input[@name=\"email\"]";
    private static final String PHONE_NUMBER_FIELD = "//input[@name=\"phoneNumber\"]";
    private static final String BIRTH_DATE_FIELD = "//input[@name=\"birthDate\"]";
    private static final String PASSWORD_FIELD = "//input[@name=\"password\"]";
    private static final String RE_PASSWORD_FIELD = "//input[@name=\"repeated_password\"]";
    private static final String CREATE_ACCOUNT_BUTTON = "//button[@type=\"submit\"]";


    @FindBy(xpath = USER_NAME_FIELD)
    private WebElement userNameField;

    @FindBy(xpath = EMAIL_FIELD)
    private WebElement emailField;

    @FindBy(xpath = PHONE_NUMBER_FIELD)
    private WebElement phoneNumberField;

    @FindBy(xpath = BIRTH_DATE_FIELD)
    private WebElement birthDateField;

    @FindBy(xpath = PASSWORD_FIELD)
    private WebElement passwordField;

    @FindBy(xpath = RE_PASSWORD_FIELD)
    private WebElement rePasswordField;

    @FindBy(xpath = CREATE_ACCOUNT_BUTTON)
    private WebElement createAccountButton;

    public SignUpPageObject(WebDriver driver) {
        super(driver);
    }

    public void enterUserName(String name) {
        userNameField.sendKeys(name);
    }

    public void enterEmail(String email) {
        emailField.sendKeys(email);
    }

    public void enterPhoneNumber(String phone) {
        phoneNumberField.sendKeys(phone);
    }

    public void enterBirthDate(String date) {
        birthDateField.sendKeys(date);
    }

    public void enterPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void enterConfirmPassword(String rePass) {
        rePasswordField.sendKeys(rePass);
    }

    public void clickOnCreateAccountButton() {
        createAccountButton.click();
    }
}

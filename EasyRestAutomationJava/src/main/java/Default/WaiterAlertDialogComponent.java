package Default;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.function.Function;

public class WaiterAlertDialogComponent {

    private static final String SUCCESS_ALERT_MESSAGE_LOCATOR = "//div[@role='alertdialog']//p[text()='success']";
    private static final int WEB_DRIVER_WAIT_TIMEOUT_IN_SECONDS = 20;

    private WebDriverWait wait;

    public WaiterAlertDialogComponent(WebDriver driver) {

        wait = new WebDriverWait(driver, WEB_DRIVER_WAIT_TIMEOUT_IN_SECONDS);
    }

    public boolean isASuccessAlertMessageShowsUp() {

        List<WebElement> success = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(SUCCESS_ALERT_MESSAGE_LOCATOR)));

        if (success.size() == 1) {
            return true;
        } else {
            return false;
        }
    }
}
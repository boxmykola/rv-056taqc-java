package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;
import java.util.NoSuchElementException;

public class WaiterAssignedOrdersComponent extends WaiterCommonOrdersComponent {

    /* Locator of clickable WebElements. */
    private static final String START_ORDER_BUTTON_LOCATOR = "//*[contains(text(),'START ORDER')]//ancestor::button";

    public WaiterAssignedOrdersComponent(WebDriver driver, WebElement container) {
        super(driver, container);
    }

    public WaiterAssignedOrdersPageObject expandOrder(int orderId) {
        expandOrderById(orderId);
        return new WaiterAssignedOrdersPageObject(driver);
    }

    public WaiterAssignedOrdersPageObject startOrder(int orderId){
        WebElement foundOrderItem = getOrder(orderId);

        WebElement neededStartOrderButton = foundOrderItem.findElement(By.xpath(START_ORDER_BUTTON_LOCATOR));

        neededStartOrderButton.click();
        return new WaiterAssignedOrdersPageObject(driver);
    }
}
package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WaiterCommonOrdersPageObject {

    /* Locators of page components. */
    private static final String WAITER_ORDERS_CONTAINER_LOCATOR = "//main/div[contains(@class,'MuiGrid-container')]";
    private static final String WAITER_PANEL_CONTAINER_LOCATOR = "//main/header";

    /* For returning new pages. */
    protected WebDriver driver;

    @FindBy(xpath = WAITER_ORDERS_CONTAINER_LOCATOR)
    protected WebElement waiterOrdersContainer;

    @FindBy(xpath = WAITER_PANEL_CONTAINER_LOCATOR)
    protected WebElement waiterPanelContainer;

    public WaiterCommonOrdersPageObject(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public WaiterPanelComponent getWaiterPanel() {
        return new WaiterPanelComponent(driver,waiterPanelContainer);
    }

    public WaiterAlertDialogComponent getAlert() {
        return new WaiterAlertDialogComponent(driver);
    }
}

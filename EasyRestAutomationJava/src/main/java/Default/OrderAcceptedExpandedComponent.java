package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;

public class OrderAcceptedExpandedComponent {

    private static final String PICK_WAITER_RADIO_BUTTON = "//input[@name='waiters'][@value='36']";
    private static final String ASSIGN_BUTTON = "//span[text()='Assign']";

    private WebElement webElement;

    private List<WebElement> pickWaiterRadioButtons;

    private WebElement assignButton;

    public OrderAcceptedExpandedComponent(WebElement webElement) {
        this.webElement = webElement;
        pickWaiterRadioButtons = webElement.findElements(By.xpath(PICK_WAITER_RADIO_BUTTON));
        assignButton = webElement.findElement(By.xpath(ASSIGN_BUTTON));
    }

    public void pressPickWaiterRadioButton(int waiter) {
        this.pickWaiterRadioButtons.get(waiter-1).click();
    }

    public void pressAcceptButton() {
        this.assignButton.click();
    }
}
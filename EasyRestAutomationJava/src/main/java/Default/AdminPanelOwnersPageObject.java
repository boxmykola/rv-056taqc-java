package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AdminPanelOwnersPageObject extends AdminPanelCommonPageObject {

    /* Locator of page components. */
    private static final String OWNERS_TABLE_LOCATOR = "//*[@id=\"root\"]/div/main/div[2]/table";

    protected OwnersModeratorsHeaderMenuComponent ownersModeratorsHeaderMenuComponent;

    public AdminPanelOwnersPageObject(WebDriver driver) {
        super(driver);
        this.ownersModeratorsHeaderMenuComponent = new OwnersModeratorsHeaderMenuComponent(driver);
    }

    public OwnersModeratorsHeaderMenuComponent getOwnersModeratorsHeaderMenuComponent() {
        return ownersModeratorsHeaderMenuComponent;
    }

    public AdminPanelCommonOwnersModeratorsTableComponent getOwners() {
        WebElement webElement = driver.findElement(By.xpath(OWNERS_TABLE_LOCATOR));
        return new AdminPanelCommonOwnersModeratorsTableComponent(webElement);
    }
}

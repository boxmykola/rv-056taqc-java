package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class AdminLeftSidebarMenuComponent {

    private static final String USERS_BUTTON = "//*[@id=\"root\"]/div/ul/a[1]/div[2]/span";
    private static final String OWNERS_BUTTON = "//*[@id=\"root\"]/div/ul/a[2]/div[2]/span";
    private static final String MODERATORS_BUTTON = "//*[@id=\"root\"]/div/ul/a[3]/div[2]/span";
    private static final String RESTAURANTS_BUTTON = "//*[@id=\"root\"]/div/ul/a[4]/div[2]/span";

    @FindBy(xpath = USERS_BUTTON)
    protected WebElement usersButton;

    @FindBy(xpath = OWNERS_BUTTON)
    protected WebElement ownersButton;

    @FindBy(xpath = MODERATORS_BUTTON )
    protected WebElement moderatorsButton;

    @FindBy(xpath = RESTAURANTS_BUTTON )
    protected WebElement restaurantsButton;

    private WebElement webElement;

    public AdminLeftSidebarMenuComponent(WebElement element) {
        this.webElement = element;
        DefaultElementLocatorFactory elementLocatorFactory = new DefaultElementLocatorFactory(webElement);
        PageFactory.initElements(elementLocatorFactory,this);
    }

    public void clickOnUsersButton(){
        usersButton.click();
    }

    public void clickOnOwnersButton(){
        ownersButton.click();
    }

    public void clickOnModeratorsButton(){
        moderatorsButton.click();
    }

    public void clickOnRestaurantsButton(){
        restaurantsButton.click();
    }

}

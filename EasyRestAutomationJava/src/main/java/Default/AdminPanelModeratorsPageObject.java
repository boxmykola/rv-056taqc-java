package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AdminPanelModeratorsPageObject extends AdminPanelCommonPageObject {

    /*locators*/
    private static final String MODERATORS_TABLE_LOCATOR = "//*[@id=\"root\"]/div/main/div[2]/table";

    private static final String ADD_MODERATOR_BUTTON = "//*[@id=\"root\"]/div/main/a/span[1]";

    @FindBy(how = How.XPATH, using = ADD_MODERATOR_BUTTON)
    private WebElement addModeratorButton;

    protected OwnersModeratorsHeaderMenuComponent ownersModeratorsHeaderMenuComponent;

    public AdminPanelModeratorsPageObject(WebDriver driver) {
        super(driver);
        this.ownersModeratorsHeaderMenuComponent = new OwnersModeratorsHeaderMenuComponent(driver);
    }

    public OwnersModeratorsHeaderMenuComponent getOwnersModeratorsHeaderMenuComponent() {
        return ownersModeratorsHeaderMenuComponent;
    }

    public AdminPanelCommonOwnersModeratorsTableComponent getModerators() {
        WebElement webElement = driver.findElement(By.xpath(MODERATORS_TABLE_LOCATOR));
        return new AdminPanelCommonOwnersModeratorsTableComponent(webElement);
    }

    public void clickOnAddModeratorButton() {
        addModeratorButton.click();
    }
}
package Default;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class RightSidebarMenuComponent {

    private static final String MY_PERSONAL_BUTTON = "//*[@id=\"root\"]/main/div/div/div/div[2]/div/a[1]/span[1]/span";
    private static final String CURRENT_ORDERS_BUTTON = "//*[@id=\"root\"]/main/div/div/div/div[2]/div/a[2]/span[1]/span/span";
    private static final String ORDER_HISTORY_BUTTON = "//*[@id=\"root\"]/main/div/div/div/div[2]/div/a[3]/span[1]/span/span";
    private static final String MY_RESTAURANTS_BUTTON = "//*[@id=\"root\"]/main/div/div/div/div[2]/div/a[3]/span[1]/span/span";

    @FindBy(xpath = MY_PERSONAL_BUTTON)
    protected WebElement myPersonalInfoButton;

    @FindBy(xpath = CURRENT_ORDERS_BUTTON)
    protected WebElement currentOrdersButton;

    @FindBy(xpath = ORDER_HISTORY_BUTTON)
    protected WebElement orderHistoryButton;

    @FindBy(xpath = MY_RESTAURANTS_BUTTON)
    protected WebElement myRestaurantsButton;

    private WebElement webElement;

    public RightSidebarMenuComponent(WebElement element) {
        this.webElement = element;
        DefaultElementLocatorFactory elementLocatorFactory = new DefaultElementLocatorFactory(webElement);
        PageFactory.initElements(elementLocatorFactory,this);
    }

    public void clickOnMyPersonalInfoButton(){
        myPersonalInfoButton.click();
    }

    public void clickOnCurrentOrdersButton(){
        currentOrdersButton.click();
    }

    public void clickOnOrderHistoryButton(){
        orderHistoryButton.click();
    }

    public void clickOnMyRestaurantsButton(){
        myRestaurantsButton.click();
    }
}
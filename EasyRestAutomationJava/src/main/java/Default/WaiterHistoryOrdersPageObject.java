package Default;

import Default.HeaderMenuComponent;
import Default.WaiterPanelComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class WaiterHistoryOrdersPageObject extends WaiterCommonOrdersPageObject {

    public WaiterHistoryOrdersPageObject(WebDriver driver) {
        super(driver);
    }

    public WaiterHistoryOrdersComponent getWaiterOrders() {
        return new WaiterHistoryOrdersComponent(driver,waiterOrdersContainer);
   }
}

package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class ModeratorLeftSidebarMenuComponent {

    private static final String USERS_BUTTON = "//*[@id=\"root\"]/div/ul/a[1]/div[2]/span";
    private static final String OWNERS_BUTTON = "//*[@id=\"root\"]/div/ul/a[2]/div[2]/span";
    private static final String RESTAURANTS_BUTTON = "//*[@id=\"root\"]/div/ul/a[3]/div[2]/span";

    @FindBy(xpath = RESTAURANTS_BUTTON)
    protected WebElement restaurantsButton;

    @FindBy(xpath = USERS_BUTTON)
    protected WebElement usersButton;

    @FindBy(xpath = OWNERS_BUTTON)
    protected WebElement ownersButton;

    private WebElement webElement;

    public ModeratorLeftSidebarMenuComponent(WebElement element) {
        this.webElement = element;
        DefaultElementLocatorFactory elementLocatorFactory = new DefaultElementLocatorFactory(webElement);
        PageFactory.initElements(elementLocatorFactory,this);
    }

    public void clickOnUsersButton(){
        usersButton.click();
    }

    public void clickOnOwnersButton(){
        ownersButton.click();
    }

    public void clickOnRestaurantsButton(){
        restaurantsButton.click();
    }

}
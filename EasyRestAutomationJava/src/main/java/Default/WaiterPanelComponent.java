package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaiterPanelComponent {

    /* Locators of clickable WebElements. */
    private static final String ALL_TAB_LOCATOR= "//*[contains(text(),'All (')]/ancestor::a";
    private static final String ASSIGNED_TAB_LOCATOR = "//*[contains(text(),'Assigned waiter (')]/ancestor::a";
    private static final String IN_PROGRESS_TAB_LOCATOR = "//*[contains(text(),'In progress (')]/ancestor::a";
    private static final String HISTORY_TAB_LOCATOR = "//*[contains(text(),'History (')]/ancestor::a";
    private static final int WEB_DRIVER_WAIT_TIMEOUT_IN_SECONDS = 20;

    @FindBy(xpath = ALL_TAB_LOCATOR)
    private WebElement allTabElement;

    @FindBy(xpath = ASSIGNED_TAB_LOCATOR)
    private WebElement assignedTabElement;

    @FindBy(xpath = IN_PROGRESS_TAB_LOCATOR)
    private WebElement inProgressTabElement;

    @FindBy(xpath = HISTORY_TAB_LOCATOR)
    private WebElement historyTabElement;

    /* For returning new pages. */
    private WebDriver driver;

    private WebDriverWait wait;

    public WaiterPanelComponent(WebDriver driver, WebElement container) {
        DefaultElementLocatorFactory elementLocatorFactory = new DefaultElementLocatorFactory(container);

        PageFactory.initElements(elementLocatorFactory, this);
        this.driver = driver;

        wait = new WebDriverWait(driver, WEB_DRIVER_WAIT_TIMEOUT_IN_SECONDS);
    }

    public WaiterOrdersPageObject clickOnAllPanelElement() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(ALL_TAB_LOCATOR)));
        allTabElement.click();
        return new WaiterOrdersPageObject(driver);
    }

    public WaiterAssignedOrdersPageObject clickOnAssignedPanelElement() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(ASSIGNED_TAB_LOCATOR)));
        assignedTabElement.click();
        return new WaiterAssignedOrdersPageObject(driver);
    }

    public WaiterInProgressOrdersPageObject clickOnInProgressPanelElement() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IN_PROGRESS_TAB_LOCATOR)));
        inProgressTabElement.click();
        return new WaiterInProgressOrdersPageObject(driver);
    }

    public WaiterHistoryOrdersPageObject clickOnHistoryPanelElement() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(HISTORY_TAB_LOCATOR)));
        historyTabElement.click();
        return new WaiterHistoryOrdersPageObject(driver);
    }
}

package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WaiterInProgressOrdersPageObject extends WaiterCommonOrdersPageObject {

    public WaiterInProgressOrdersPageObject(WebDriver driver) {
        super(driver);
    }

    public WaiterInProgressOrdersComponent getWaiterOrders() {
        return new WaiterInProgressOrdersComponent(driver, waiterOrdersContainer);
    }
}
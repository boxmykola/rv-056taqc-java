package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class MyRestaurantsComponent {

    private static final String DETAILS_BUTTON = "//span[text()=\"details\"]";
    private static final String WATCH_MENU_BUTTON = "//span[text()=\"Watch Menu\"]";
    private static final String MORE_BUTTON_COMPONENT = "//button[@type=\"button\"]";

    private WebElement webElement;

    private WebDriver driver;

    @FindBy(xpath = DETAILS_BUTTON)
    protected WebElement detailsButton;

    @FindBy(xpath = WATCH_MENU_BUTTON)
    protected WebElement watchMenuButton;

    @FindBy(xpath = MORE_BUTTON_COMPONENT)
    protected WebElement moreButtonComponent;

    public MyRestaurantsComponent(WebElement element, WebDriver driver ) {
        this.webElement = element;
        this.driver = driver;
        DefaultElementLocatorFactory elementLocatorFactory = new DefaultElementLocatorFactory(webElement);
        PageFactory.initElements(elementLocatorFactory, this);
    }

    public MyRestaurantsMoreButtonListComponent getMoreButtonListComponent(){
        return new MyRestaurantsMoreButtonListComponent(driver, moreButtonComponent);
    }

    public void clickOnDetailsButton() {
        detailsButton.click();
    }

    public void clickOnWatchMenuButton() {
        watchMenuButton.click();
    }

    public void clickMoreButtonComponent() {
        moreButtonComponent.click();
    }
}
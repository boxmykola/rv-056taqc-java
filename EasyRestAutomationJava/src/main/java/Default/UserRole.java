package Default;

public enum UserRole {
    CLIENT,
    OWNER,
    MODERATOR,
    ADMIN,
    ADMINISTRATOR,
    WAITER
}

package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WaiterOrdersComponent extends WaiterCommonOrdersComponent {

    public WaiterOrdersComponent(WebDriver driver, WebElement container) {
        super(driver, container);
    }

    public WaiterOrdersPageObject expandOrder(int orderId) {
        expandOrderById(orderId);
        return new WaiterOrdersPageObject(driver);
    }

}
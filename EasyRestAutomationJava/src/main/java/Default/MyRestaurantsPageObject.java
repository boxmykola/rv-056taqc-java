package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyRestaurantsPageObject extends CommonPageObject {

    private static final String MY_RESTAURANTS_LIST_COMPONENT =  "//*[contains(@class, \"RestaurantListItem-card-\")]";
    private static final String CREATE_NEW_RESTAURANTS_COMPONENT = "//div[contains(@class, 'MuiCollapse-wrapperInner-')]/..";
    private static final String ADD_BUTTON = "//button[@title = \"Add restaurant\"]";

    @FindBy(xpath = ADD_BUTTON)
    protected WebElement addButton;

    @FindBy(xpath = MY_RESTAURANTS_LIST_COMPONENT)
    protected WebElement listMyRestaurants;

    @FindBy(xpath = CREATE_NEW_RESTAURANTS_COMPONENT)
    protected WebElement createNewRestaurantsComponent;

    public MyRestaurantsPageObject(WebDriver driver) {
        super(driver);
    }

    public MyRestaurantsListComponent getRestaurants() {
        return new MyRestaurantsListComponent(driver, listMyRestaurants);
    }

    public MyRestaurantsCreateNewRestaurantComponent getNewRestaurants() {
        return new MyRestaurantsCreateNewRestaurantComponent(driver, createNewRestaurantsComponent);
    }

    public void clickAddButton() {
        addButton.click();
    }
}
package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HeaderMenuCommonComponent extends CommonPageObject{

    private static final String HOME_BUTTON = "//a/span[text()='Home']";
    private static final String RESTAURANTS_BUTTON = "//a/span[text()='Restaurants List']";

    @FindBy(how = How.XPATH, using = HOME_BUTTON)
    private WebElement homeButton;

    @FindBy(how = How.XPATH, using = RESTAURANTS_BUTTON)
    private WebElement restaurantsButton;

    public HeaderMenuCommonComponent(WebDriver driver) {
        super(driver);
    }

    public void clickOnHomeButton() {
        homeButton.click();
    }

    public void clickOnRestaurantsButton() {
        restaurantsButton.click();
    }

}

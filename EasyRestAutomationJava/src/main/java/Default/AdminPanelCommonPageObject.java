package Default;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class AdminPanelCommonPageObject {
    protected WebDriver driver;
    public AdminPanelCommonPageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
}
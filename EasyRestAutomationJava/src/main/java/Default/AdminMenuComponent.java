package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AdminMenuComponent extends UserMenuCommonComponent{

    private static final String ADMIN_PANEL_BUTTON = "//a[text()='Admin Panel']";

    @FindBy(how = How.XPATH, using = ADMIN_PANEL_BUTTON)
    private WebElement adminPanelButton;

    public AdminMenuComponent(WebDriver driver) {
        super(driver);
    }

    public void clickOnAdminPanelButton() {
        adminPanelButton.click();
    }

}

package Default;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HeaderMenuComponent extends HeaderMenuCommonComponent{

    private static final String SIGN_IN_BUTTON = "//a/span[text()='Sign In']";
    private static final String SIGN_UP_BUTTON = "//a/span[text()='Sign Up']";

    @FindBy(how = How.XPATH, using = SIGN_IN_BUTTON)
    private WebElement signInButton;

    @FindBy(how = How.XPATH, using = SIGN_UP_BUTTON)
    private WebElement signUpButton;

    public HeaderMenuComponent(WebDriver driver) {
        super(driver);
    }

    public SignInPageObject clickOnSignInButton() {
        signInButton.click();
        return new SignInPageObject(driver);
    }

    public SignUpPageObject clickOnSignUpButton() {
        signUpButton.click();
        return new SignUpPageObject(driver);
    }

}

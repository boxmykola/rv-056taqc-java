package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePageForLoggedInUserPageObject extends CommonPageObject{

    private static final String RESTAURANTS_CATEGORIES_CONTAINER = "//div[contains(@class,'MuiGrid-container')]";

    @FindBy(how = How.XPATH, using = RESTAURANTS_CATEGORIES_CONTAINER)
    private WebElement container;

    private HeaderMenuForLoggedInUserComponent headerMenuComponent;

    private RestaurantsCategoriesOnHomePageComponent restaurantsCategoriesOnHomePageComponent;

    public HomePageForLoggedInUserPageObject(WebDriver driver) {
        super(driver);
        this.headerMenuComponent = new HeaderMenuForLoggedInUserComponent(driver);
        this.restaurantsCategoriesOnHomePageComponent = new RestaurantsCategoriesOnHomePageComponent(driver, container);
    }

    public HeaderMenuForLoggedInUserComponent getHeaderMenuComponentForLoggedInUser(){
        return headerMenuComponent;
    }

    public RestaurantsCategoriesOnHomePageComponent getRestaurantsCategoriesComponent() {
        return restaurantsCategoriesOnHomePageComponent;
    }

}

package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.*;

public class RestaurantsCategoriesOnHomePageComponent extends CommonPageObject {

    private static final String RESTAURANTS_CATEGORIES_BUTTONS = "//div[contains(@class,'MuiGrid-item')]";

    private WebElement container;

    public RestaurantsCategoriesOnHomePageComponent(WebDriver driver, WebElement container) {
        super(driver);
        this.container = container;
    }

    private List<WebElement> getListOfRestaurantsCategories() {
        return container.findElements(By.xpath(RESTAURANTS_CATEGORIES_BUTTONS));
    }

    public List<RestaurantCategoryOnHomePageComponent> getRestaurantsList() {
        List<RestaurantCategoryOnHomePageComponent> listOfRestaurantsCategories = new ArrayList<>();

        List<WebElement> restaurantsCategories = getListOfRestaurantsCategories();

        for (WebElement webElement : restaurantsCategories) {
            listOfRestaurantsCategories.add(new RestaurantCategoryOnHomePageComponent(webElement));
        }

        return listOfRestaurantsCategories;
    }

    public void clickOnRestaurantsCategory(String restaurantCategoryName) {
        List<WebElement> restaurantsCategories = getListOfRestaurantsCategories();
        
        ListIterator<WebElement> iterator = restaurantsCategories.listIterator();

        while (iterator.hasNext()) {
            if (iterator.next().getText().equalsIgnoreCase(restaurantCategoryName)) {
                iterator.previous().click();
                break;
            } else if (!iterator.hasNext()) {
                throw new NoSuchElementException("There is not such element");
            }
        }
    }

}

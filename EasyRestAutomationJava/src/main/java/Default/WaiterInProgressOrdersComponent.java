package Default;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;
import java.util.function.Function;

public class WaiterInProgressOrdersComponent extends WaiterCommonOrdersComponent {

    /* Locator of clickable WebElement. */
    private static final String CLOSE_ORDER_BUTTON_LOCATOR = "//*[contains(text(),'Close order')]//ancestor::button";

    public WaiterInProgressOrdersComponent(WebDriver driver, WebElement container) {
        super(driver, container);
    }

    public WaiterInProgressOrdersPageObject expandOrder(int orderId) {
        expandOrderById(orderId);
        return new WaiterInProgressOrdersPageObject(driver);
    }

    public WaiterInProgressOrdersPageObject closeOrder(int orderId) {
        WebElement foundOrderItem = getOrder(orderId);

        FluentWait<WebElement> wait = new FluentWait<WebElement>(foundOrderItem)
                .withTimeout(Duration.ofSeconds(FLUENT_WAIT_TIMEOUT_IN_SECONDS))
                .pollingEvery(Duration.ofSeconds(FLUENT_WAIT_POLLING_IN_SECONDS))
                .ignoring(org.openqa.selenium.NoSuchElementException.class);

        java.util.function.Function<WebElement, WebElement> clickableCloseButton = new Function<WebElement, WebElement>() {
            public WebElement apply(WebElement webElement) {

                WebElement displayedElement = null;

                try {
                    WebElement locatedElement = webElement.findElement(By.xpath(CLOSE_ORDER_BUTTON_LOCATOR));
                    displayedElement = locatedElement.isDisplayed() ? locatedElement: null;
                } catch (StaleElementReferenceException exceptionObject) {
                    return null;
                }

                try {
                    return displayedElement != null && displayedElement.isEnabled() ? displayedElement : null;
                } catch (StaleElementReferenceException exceptionObject) {
                    return null;
                }
            }
        };

        WebElement orderItemCloseClickableButton = wait.until(clickableCloseButton);

        orderItemCloseClickableButton.click();

        return new WaiterInProgressOrdersPageObject(driver);
    }
}
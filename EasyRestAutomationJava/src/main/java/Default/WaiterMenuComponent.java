package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class WaiterMenuComponent extends UserMenuCommonComponent{

    private static final String WAITER_PANEL_BUTTON = "//a[text()='Waiter panel']";

    @FindBy(how = How.XPATH, using = WAITER_PANEL_BUTTON)
    private WebElement waiterPanelButton;

    public WaiterMenuComponent(WebDriver driver) {
        super(driver);
    }

    public WaiterOrdersPageObject clickOnWaiterPanelButton() {
        waiterPanelButton.click();
        return new WaiterOrdersPageObject(driver);
    }

}

package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class UserMenuComponent extends UserMenuCommonComponent{

    private static final String MY_PROFILE_BUTTON = "//a[text()='My Profile']";

    @FindBy(how = How.XPATH, using = MY_PROFILE_BUTTON)
    private WebElement myProfileButton;

    public UserMenuComponent(WebDriver driver) {
        super(driver);
    }

    public void clickMyProfileButton() {
        myProfileButton.click();
    }

}

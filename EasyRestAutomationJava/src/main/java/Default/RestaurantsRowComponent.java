package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class RestaurantsRowComponent {

    private static final String PARENT_RESTAURANT_LOCATOR = "./[1]/th";

    @FindBy(xpath = PARENT_RESTAURANT_LOCATOR)
    private WebElement restaurantName;

    private WebElement parent;

    public RestaurantsRowComponent(WebElement parent) {
        this.parent = parent;
        DefaultElementLocatorFactory elementLocatorFactory = new DefaultElementLocatorFactory(parent);
        PageFactory.initElements(elementLocatorFactory,this);
    }

    public String getName() {
        return restaurantName.getText();
    }
}

package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OwnersModeratorsHeaderMenuComponent extends AdminPanelCommonPageObject {

    /* Locators of page components. */
    private static final String All_BUTTON = "//*[@id=\"root\"]/div/main/div[1]/header/div/div[2]/div[2]/div/button[1]/span[1]/span/span";

    private static final String ACTIVE_BUTTON = "//*[@id=\"root\"]/div/main/div[1]/header/div/div[2]/div[2]/div/button[2]/span[1]/span";

    private static final String BANNED_BUTTON = "//*[@id=\"root\"]/div/main/div[1]/header/div/div[2]/div[2]/div/button[3]/span[1]/span/span";

    @FindBy(how = How.XPATH, using = All_BUTTON)
    private WebElement allButton;

    @FindBy(how = How.XPATH, using = ACTIVE_BUTTON)
    private WebElement activeButton;

    @FindBy(how = How.XPATH, using = BANNED_BUTTON)
    private WebElement bannedButton;

    public OwnersModeratorsHeaderMenuComponent(WebDriver driver) {
        super(driver);
    }

    public void clickOnAllButton() {
        allButton.click();
    }

    public void clickOnActiveButton() {
        activeButton.click();
    }

    public void clickOnBannedButton() {
        bannedButton.click();
    }
}

package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WaiterHistoryOrdersComponent extends WaiterCommonOrdersComponent {

    public WaiterHistoryOrdersComponent(WebDriver driver, WebElement container) {
        super(driver, container);
    }

    public WaiterHistoryOrdersPageObject expandOrder(int orderId) {
        expandOrderById(orderId);
        return new WaiterHistoryOrdersPageObject(driver);
    }

}
package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OwnerMenuComponent extends UserMenuCommonComponent{

    private static final String MY_RESTAURANTS_BUTTON = "//a[text()='My restaurants']";

    @FindBy(how = How.XPATH, using = MY_RESTAURANTS_BUTTON)
    private WebElement myRestaurantsButton;

    public OwnerMenuComponent(WebDriver driver) {
        super(driver);
    }

    public void clickOnMyRestaurantsButton() {
        myRestaurantsButton.click();
    }

}

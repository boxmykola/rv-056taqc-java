package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.List;

public class AdminPanelRestaurantsTableComponent {

    private WebElement element;

    public AdminPanelRestaurantsTableComponent(WebElement element) {
        this.element = element;
    }

    public List<RestaurantsRowComponent> getRows() {
        String ROW_TAG_NAME = "tr";
        List<WebElement> rows = element.findElements(By.tagName(ROW_TAG_NAME));
        List<RestaurantsRowComponent> components = new ArrayList<>();
        for (WebElement row : rows) {
            components.add(new RestaurantsRowComponent(row));
        }
        return components;
    }
}

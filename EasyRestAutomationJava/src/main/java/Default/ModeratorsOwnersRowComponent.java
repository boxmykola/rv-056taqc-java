package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class ModeratorsOwnersRowComponent {

    private static final String PARENT_USER_NAME_LOCATOR = "./[1]/th";
    private static final String PARENT_BAN_UNBAN_BUTTON_LOCATOR = "./td[5]/button";

    @FindBy(xpath = PARENT_USER_NAME_LOCATOR)
    private WebElement userName;

    @FindBy(xpath = PARENT_BAN_UNBAN_BUTTON_LOCATOR)
    private WebElement banUnbanButton;

    private WebElement parent;

    public ModeratorsOwnersRowComponent(WebElement parent) {
        this.parent = parent;
        DefaultElementLocatorFactory elementLocatorFactory = new DefaultElementLocatorFactory(parent);
        PageFactory.initElements(elementLocatorFactory,this);
    }

    public String getUserName() {
        return userName.getText();
    }

    public void clickOnBanUnbanButton() {
        banUnbanButton.click();
    }

    public boolean isActive() {
        return banUnbanButton.getAttribute("class").contains("colorPrimary");
    }

    public boolean isBanned() {
        return banUnbanButton.getAttribute("class").contains("colorSecondary");
    }
}

package Default;

import Default.HeaderMenuComponent;
import Default.WaiterPanelComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class WaiterAssignedOrdersPageObject extends WaiterCommonOrdersPageObject {

    public WaiterAssignedOrdersPageObject(WebDriver driver) {
        super(driver);
    }

    public WaiterAssignedOrdersComponent getWaiterOrders() {
        return new WaiterAssignedOrdersComponent(driver,waiterOrdersContainer);
    }
}

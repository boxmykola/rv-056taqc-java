package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class MyProfilePagePageObject extends CommonPageObject {

    private static final String EMAIL_ADDRESS = "//th[text()='Email:']/following-sibling::td";

    @FindBy(how = How.XPATH, using = EMAIL_ADDRESS)
    private WebElement emailAddressTextField;

    private HeaderMenuForLoggedInUserComponent headerMenuForLoggedInUserComponent;

    public MyProfilePagePageObject(WebDriver driver) {
        super(driver);
        this.headerMenuForLoggedInUserComponent = new HeaderMenuForLoggedInUserComponent(driver);
    }

    public String getEmailAddress(){
        return emailAddressTextField.getText();
    }

    public HeaderMenuForLoggedInUserComponent getHeaderMenuForLoggedInUserComponent() {
        return headerMenuForLoggedInUserComponent;
    }

}

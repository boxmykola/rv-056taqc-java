package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePageForNotLoggedInUserPageObject extends CommonPageObject {

    private static final String RESTAURANTS_CATEGORIES_CONTAINER = "//div[contains(@class,'MuiGrid-container')]";

    @FindBy(how = How.XPATH, using = RESTAURANTS_CATEGORIES_CONTAINER)
    private WebElement container;

    private HeaderMenuComponent headerMenuComponent;

    private RestaurantsCategoriesOnHomePageComponent restaurantsCategoriesOnHomePageComponent;

    public HomePageForNotLoggedInUserPageObject(WebDriver driver) {
        super(driver);
        this.headerMenuComponent = new HeaderMenuComponent(driver);
        this.restaurantsCategoriesOnHomePageComponent = new RestaurantsCategoriesOnHomePageComponent(driver, container);
    }

    public HeaderMenuComponent getHeaderMenuComponentForNotLoggedInUser(){
        return headerMenuComponent;
    }

    public RestaurantsCategoriesOnHomePageComponent getRestaurantsCategoriesComponent() {
        return restaurantsCategoriesOnHomePageComponent;
    }

}

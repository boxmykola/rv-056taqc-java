package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HeaderMenuForLoggedInUserComponent extends HeaderMenuCommonComponent {
    private WebDriverWait driverWait;

    private static final String USER_MENU_BUTTON = "//span/div[contains(@class,'MuiAvatar')]";
    private final int timeOutInSeconds = 10;

    @FindBy(how = How.XPATH, using = USER_MENU_BUTTON)
    private WebElement userMenuButton;

    public HeaderMenuForLoggedInUserComponent(WebDriver driver) {
        super(driver);
        driverWait = new WebDriverWait(driver, timeOutInSeconds);
    }

    public <T extends UserMenuCommonComponent> T clickOnUserMenuButton(UserRole userRole, Class<T> type) {
        userMenuButton.click();
        switch (userRole) {
            case CLIENT:
                return type.cast(new UserMenuComponent(driver));
            case OWNER:
                return type.cast(new OwnerMenuComponent(driver));
            case MODERATOR:
                return type.cast(new ModeratorMenuComponent(driver));
            case ADMIN:
                return type.cast(new AdminMenuComponent(driver));
            case ADMINISTRATOR:
                return type.cast(new AdministratorMenuComponent(driver));
            case WAITER:
                return type.cast(new WaiterMenuComponent(driver));
            default:
                return null;
        }
    }

    public boolean visibilityOfUserIcon() {
        return driverWait.until(ExpectedConditions.visibilityOf(userMenuButton))
                .isDisplayed();
    }
}

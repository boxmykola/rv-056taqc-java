package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class MyRestaurantsMoreButtonComponent {

    private static final String ARCHIVE_BUTTON = "//span[text()=\"Archive\"]";
    private static final String MANAGE_BUTTON = "//span[text()=\"Manage\"]";

    @FindBy(xpath = ARCHIVE_BUTTON)
    protected WebElement archiveButton;

    @FindBy(xpath = MANAGE_BUTTON)
    protected WebElement manageButton;

    private WebDriver driver;

    private WebElement webElement;

    public MyRestaurantsMoreButtonComponent(WebElement element) {
        this.webElement = element;
        DefaultElementLocatorFactory elementLocatorFactory = new DefaultElementLocatorFactory(webElement);
        PageFactory.initElements(elementLocatorFactory, this);
    }

    public void clickArchiveButton() {
        archiveButton.click();
    }

    public void clickManageButton() {
        manageButton.click();
    }
}
package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.List;
public class MyRestaurantsListComponent extends CommonPageObject {

    private WebElement webElement;

    private static final String RESTAURANTS_LIST = "//*[contains(@class, \"RestaurantListItem-card-\")]";

    public MyRestaurantsListComponent(WebDriver driver, WebElement element) {
        super(driver);
        this.webElement = element;
        this.driver = driver;
    }

    public ArrayList<MyRestaurantsComponent> getMyRestaurantsList() {

        ArrayList<MyRestaurantsComponent> list = new ArrayList<>();
        List<WebElement> restaurantsList = webElement.findElements(By.xpath(RESTAURANTS_LIST));

        for (WebElement element : restaurantsList) {
            list.add(new MyRestaurantsComponent(element, driver));
        }

        return list;
    }
}
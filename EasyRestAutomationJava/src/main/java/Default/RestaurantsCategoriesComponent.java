package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RestaurantsCategoriesComponent extends CommonPageObject {

    private static final String SCROLL_BUTTON = "//button[@type=\"button\"]";
    private static final String CATEGORY_NAME = "//span[contains(@class, \"MuiTab-label-\")]";
    private WebElement webElement;

    @FindBy(xpath = SCROLL_BUTTON)
    private WebElement scrollButton;

    public RestaurantsCategoriesComponent(WebDriver driver, WebElement element) {
        super(driver);
        this.webElement = element;
    }

    public void clickOnScrollButton() {
        scrollButton.click();
    }

    public RestaurantsListPageObject clickOnCategory(String name) {
        List<WebElement> categoriesRestaurants = webElement.findElements(By.xpath(CATEGORY_NAME));
        WebElement foundElement = null;

        for (WebElement element : categoriesRestaurants) {

            String categoryName = element.getText();

            if (name.equalsIgnoreCase(categoryName)) {
                foundElement = element;
                break;
            }
        }

        if (foundElement != null) {
            foundElement.click();
        }

        return new RestaurantsListPageObject(driver);
    }
}

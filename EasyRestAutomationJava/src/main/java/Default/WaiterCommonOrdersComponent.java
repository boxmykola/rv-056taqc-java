package Default;

import org.openqa.selenium.*;

import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;
import java.util.function.Function;

public class WaiterCommonOrdersComponent {

    protected static final int FLUENT_WAIT_TIMEOUT_IN_SECONDS = 20;
    protected static final int FLUENT_WAIT_POLLING_IN_SECONDS = 1;

    /* Locators of clickable WebElements. */
    private static final String EXPAND_BUTTON_LOCATOR = "//*[local-name()='svg']/ancestor::button[@aria-expanded='false']";
    private static final String ORDER_ITEM_LOCATOR = "//*[contains(text(),'№')]/ancestor::div[contains(@class,'MuiGrid-item')]";

    /* For chain/fluent pattern. */
    protected WebDriver driver;

    private FluentWait<WebElement> wait;

    public WaiterCommonOrdersComponent(WebDriver driver, WebElement container) {

        /* Save driver for all descendants */
        this.driver = driver;

        wait = new FluentWait<WebElement>(container)
                .withTimeout(Duration.ofSeconds(FLUENT_WAIT_TIMEOUT_IN_SECONDS))
                .pollingEvery(Duration.ofSeconds(FLUENT_WAIT_POLLING_IN_SECONDS))
                .ignoring(org.openqa.selenium.NoSuchElementException.class);
    }

    protected void expandOrderById(int orderId) {

        /*  Generating expand button locator for selected order. */
        final String DYNAMIC_LOCATOR = "//*[contains(text(),'№" + orderId + " |')]/ancestor::div"
                + "[contains(@class,'MuiGrid-item')]//*[local-name()='svg']/ancestor::button[@aria-expanded='false']";

        java.util.function.Function<WebElement, WebElement> expandClickableButton = new Function<WebElement, WebElement>() {
            public WebElement apply(WebElement webElement) {

                WebElement displayedElement = null;

                try {
                    WebElement locatedElement = webElement.findElement(By.xpath(DYNAMIC_LOCATOR));
                    displayedElement = locatedElement.isDisplayed() ? locatedElement: null;
                } catch (StaleElementReferenceException exceptionObject) {
                    return null;
                }

                try {
                    return displayedElement != null && displayedElement.isEnabled() ? displayedElement : null;
                } catch (StaleElementReferenceException exceptionObject) {
                    return null;
                }
            }
        };

        WebElement expandOrderClickableButton = wait.until(expandClickableButton);
        expandOrderClickableButton.click();
    }

    protected WebElement getOrder(int orderId) {

        /*  Generating order id locator. */
        final String DYNAMIC_LOCATOR = "//*[contains(text(),'№" + orderId + " |')]/ancestor::div[contains(@class,'MuiGrid-item')]";

        java.util.function.Function<WebElement, WebElement> findOrderItem = new Function<WebElement, WebElement>() {
            public WebElement apply(WebElement driver) {
                return driver.findElement(By.xpath(DYNAMIC_LOCATOR));
            }
        };

        WebElement foundOrderItem = wait.until(findOrderItem);

        return foundOrderItem;
    }
}

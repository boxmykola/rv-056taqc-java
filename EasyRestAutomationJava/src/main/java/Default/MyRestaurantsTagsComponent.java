package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class MyRestaurantsTagsComponent {
    private static final String TAG_BEER = "//*[@id=\"menu-tags\"]/div[2]/ul/li[1]/div/span";
    private static final String TAG_KEBAB = "//*[@id=\"menu-tags\"]/div[2]/ul/li[2]/div/span";
    private static final String TAG_PIZZA = "//*[@id=\"menu-tags\"]/div[2]/ul/li[3]/div/span";
    private static final String TAG_TURKISH_CUISINE = "//*[@id=\"menu-tags\"]/div[2]/ul/li[4]/div/span";
    private static final String TAG_VEGETARIAN = "//*[@id=\"menu-tags\"]/div[2]/ul/li[5]/div/span";
    private static final String TAG_SUSHI = "//*[@id=\"menu-tags\"]/div[2]/ul/li[6]/div/span";
    private static final String TAG_FAST_FOOD = "//*[@id=\"menu-tags\"]/div[2]/ul/li[7]/div/span";
    private static final String TAG_PUB = "//*[@id=\"menu-tags\"]/div[2]/ul/li[8]/div/span";
    private static final String TAG_UKRAINIAN_CUSINE = "//*[@id=\"menu-tags\"]/div[2]/ul/li[9]/div/span";
    private static final String TAG_BURGERS = "//*[@id=\"menu-tags\"]/div[2]/ul/li[10]/div/span";
    private static final String TAG_JAPANESE_CUSINE = "//*[@id=\"menu-tags\"]/div[2]/ul/li[11]/div/span";
    private static final String TAG_GREEL = "//*[@id=\"menu-tags\"]/div[2]/ul/li[12]/div/span";
    private static final String TAG_COFFE_TEA = "//*[@id=\"menu-tags\"]/div[2]/ul/li[13]/div/span";

    private WebElement webElement;

    public MyRestaurantsTagsComponent(WebElement element) {
        this.webElement = element;
        DefaultElementLocatorFactory elementLocatorFactory = new DefaultElementLocatorFactory(webElement);
        PageFactory.initElements(elementLocatorFactory, this);
    }

    @FindBy(xpath = TAG_BEER)
    protected WebElement tagBeer;

    @FindBy(xpath = TAG_KEBAB)
    protected WebElement tagKebab;

    @FindBy(xpath = TAG_PIZZA)
    protected WebElement tagPizza;

    @FindBy(xpath = TAG_TURKISH_CUISINE)
    protected WebElement tagTurkishCuisine;

    @FindBy(xpath = TAG_VEGETARIAN)
    protected WebElement tagVegetarian;

    @FindBy(xpath = TAG_SUSHI)
    protected WebElement tagSushi;

    @FindBy(xpath = TAG_FAST_FOOD)
    protected WebElement tagFastFood;

    @FindBy(xpath = TAG_PUB)
    protected WebElement tagPub;

    @FindBy(xpath = TAG_UKRAINIAN_CUSINE)
    protected WebElement tagUkrainianCusine;

    @FindBy(xpath = TAG_BURGERS)
    protected WebElement tagBurgers;

    @FindBy(xpath = TAG_JAPANESE_CUSINE)
    protected WebElement tagJapaneseCusine;

    @FindBy(xpath = TAG_GREEL)
    protected WebElement tagGreel;

    @FindBy(xpath = TAG_COFFE_TEA)
    protected WebElement tagCoffeTea;

    public void clickOnTagBeer() {
        if (!tagBeer.isSelected())
            tagBeer.click();
    }

    public void clickOnTagKebab() {
        if (!tagKebab.isSelected())
            tagKebab.click();
    }

    public void clickOnTagPizza() {
        if (!tagPizza.isSelected())
            tagPizza.click();
    }

    public void clickOnTagTurkishCuisine() {
        if (!tagTurkishCuisine.isSelected())
            tagTurkishCuisine.click();
    }

    public void clickOnTagVegetarian() {
        if (!tagVegetarian.isSelected())
            tagVegetarian.click();
    }

    public void clickOnTagSushi() {
        if (!tagSushi.isSelected())
            tagSushi.click();
    }

    public void clickOnTagFastFood() {
        if (!tagFastFood.isSelected())
            tagFastFood.click();
    }

    public void clickOnTagPub() {
        if (!tagPub.isSelected())
            tagPub.click();
    }

    public void clickOnTagUkrainianCusine() {
        if (!tagUkrainianCusine.isSelected())
            tagUkrainianCusine.click();
    }

    public void clickOnTagBurgers() {
        if (!tagBurgers.isSelected())
            tagBurgers.click();
    }

    public void clickOnTagJapaneseCusine() {
        if (!tagJapaneseCusine.isSelected())
            tagJapaneseCusine.click();
    }

    public void clickOnTagGreel() {
        if (!tagGreel.isSelected())
            tagGreel.click();
    }

    public void clickOnTagCoffeTea() {
        if (!tagCoffeTea.isSelected())
            tagCoffeTea.click();
    }
}
package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ModeratorMenuComponent extends UserMenuCommonComponent{

    private static final String MODERATOR_PANEL_BUTTON = "//a[text()='Moderator panel']";

    @FindBy(how = How.XPATH, using = MODERATOR_PANEL_BUTTON)
    private WebElement moderatorPanelButton;

    public ModeratorMenuComponent(WebDriver driver) {
        super(driver);
    }

    public void clickOnModeratorPanelButton() {
        moderatorPanelButton.click();
    }

}

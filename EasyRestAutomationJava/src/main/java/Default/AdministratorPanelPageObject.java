package Default;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.ArrayList;
import java.util.List;

public class AdministratorPanelPageObject extends CommonPageObject {

    private static final String ORDER_SELECTOR = "//div[@class='MuiPaper-root-10 MuiPaper-elevation1-13 MuiPaper-rounded-11 MuiExpansionPanel-root-456 MuiExpansionPanel-rounded-457 ExpandItem-wrapper-454']";
    private static final String WAITING_FOR_CONFIRM_BUTTON = "//span[contains (text(), 'Waiting for confirm')]";
    private static final String ACCEPTED_BUTTON = "//span[contains (text(), 'Accepted')]";
    private static final String ASSIGNED_WAITER_BUTTON = "//span[contains (text(), 'Assigned waiter')]";
    private static final String WAITERS_BUTTONS = "//span[contains (text(), 'Waiters')]";

    @FindBy(xpath = WAITING_FOR_CONFIRM_BUTTON)
    private WebElement waitingForConfirmButton;

    @FindBy(xpath = ACCEPTED_BUTTON)
    private WebElement acceptedButton;

    @FindBy(xpath = ASSIGNED_WAITER_BUTTON)
    private WebElement assignedWaiterButton;

    @FindBy(xpath = WAITERS_BUTTONS)
    private WebElement waitersButton;

    public AdministratorPanelPageObject(WebDriver driver) {
	super(driver);
    }

    /**
     * clicks on the  waiting to confirm tab
     * returns the list of orders
     */
    public ArrayList<OrderWaitingToConfirmCollapsedComponent> pressWaitingForConfirmButton() {
	this.waitingForConfirmButton.click();
	return waitingToConfirmCollapsedComponents();
    }

    /**
     * clicks on the accepted tab
     * returns the list of orders
     */
    public ArrayList<OrderAcceptedCollapsedComponent> pressAcceptedButton() {
	this.acceptedButton.click();
	return acceptedCollapsedComponents();
    }

    /**
     * runs through the container
     * returns the components for waiting to confirm tab
     */
    private ArrayList<OrderWaitingToConfirmCollapsedComponent> waitingToConfirmCollapsedComponents() {
	WebElement parentContainer = driver.findElement(By.xpath(ORDER_SELECTOR));
	List<WebElement> divs = parentContainer.findElements(By.tagName("div"));

	ArrayList<OrderWaitingToConfirmCollapsedComponent> waitingToConfirmComponents = new ArrayList<>();
	divs.forEach(div -> waitingToConfirmComponents.add(new OrderWaitingToConfirmCollapsedComponent(div)));
	return waitingToConfirmComponents;
    }

    /**
     * runs through the container
     * returns the components for accepted tab
     */
    private ArrayList<OrderAcceptedCollapsedComponent> acceptedCollapsedComponents() {
	WebElement parentContainer = driver.findElement(By.xpath(ORDER_SELECTOR));
	List<WebElement> divs = parentContainer.findElements(By.tagName("div"));

	ArrayList<OrderAcceptedCollapsedComponent> acceptedComponents = new ArrayList<>();
	divs.forEach(div -> acceptedComponents.add(new OrderAcceptedCollapsedComponent(div)));
	return acceptedComponents;
    }
}
package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.html5.WebStorage;
import org.testng.Assert;
import org.testng.annotations.*;

public class WaiterPanelCloseOrderTest {
    private static final String APPLICATION_URL = "http://localhost:3000/";
    private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
    private static final String CHROME_DRIVER_LOCATION = "src/test/resources/Driver/Chrome/86.0.4240.22/win32/chromedriver.exe";

    private WebDriver driver;

    @BeforeTest()
    public void startDriver() {
	System.setProperty(CHROME_DRIVER_PROPERTY, CHROME_DRIVER_LOCATION);
	driver = new ChromeDriver();
	driver.manage().window().maximize();
    }

    public void openTheApplication() {
	driver.get(APPLICATION_URL);
    }

    @DataProvider(name = "testDataCloseOrderAsWaiter")
    public Object[][] testData() {
	return new Object[][] {
		{new String("alexandriawright@test.com"), new String("1"), 124},
	};
    }

    @Test(dataProvider = "testDataCloseOrderAsWaiter")
    public void loginAsWaiterAndCloseHisOrderById(String testEmail, String testPassword, int testOrder) {
	HomePageForNotLoggedInUserPageObject homePage = new HomePageForNotLoggedInUserPageObject(driver);

	openTheApplication();

	SignInPageObject signPage = homePage
		.getHeaderMenuComponentForNotLoggedInUser()
		.clickOnSignInButton();

	signPage.enterEmail(testEmail);
	signPage.enterPassword(testPassword);
	signPage.clickOnButtonSignIn();

	WaiterOrdersPageObject waiterOrdersPage = new WaiterOrdersPageObject(driver);

	WaiterInProgressOrdersPageObject waiterInProgressOrdersPage = waiterOrdersPage
		.getWaiterPanel()
		.clickOnInProgressPanelElement();

	waiterInProgressOrdersPage
		.getWaiterOrders()
		.expandOrder(testOrder)
		.getWaiterOrders()
		.closeOrder(testOrder);

	boolean actualASuccessAlertMessageShowsUp = waiterInProgressOrdersPage
		.getAlert()
		.isASuccessAlertMessageShowsUp();

	final boolean EXPECTED_A_SUCCESS_ALERT_MESSAGE_SHOWS_UP = true;
	boolean order_is_closed = EXPECTED_A_SUCCESS_ALERT_MESSAGE_SHOWS_UP == actualASuccessAlertMessageShowsUp;

	Assert.assertTrue(order_is_closed);
    }

    @AfterMethod
    public void logOut() {
        if (driver != null) {
	    if (driver instanceof WebStorage) {
		WebStorage webStorage = (WebStorage)driver;
		webStorage.getSessionStorage().clear();
		webStorage.getLocalStorage().clear();
	    }

	    driver.manage().deleteAllCookies();
	}
    }

    @AfterTest(alwaysRun = true)
    public void stopDriver() {
	if (driver != null) {
	    driver.quit();
	}
    }
}
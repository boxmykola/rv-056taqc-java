package Default;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class SignInTest {
    private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
    private static final String CHROME_DRIVER_LOCATION = "src/test/resources/Driver/Chrome/85.0.4183.87/win32/chromedriver.exe";
    private static final String APPLICATION_URL = "http://localhost:3000/";
    private WebDriver driver;

    @BeforeClass
    public void setupClass() {
        System.setProperty(CHROME_DRIVER_PROPERTY, CHROME_DRIVER_LOCATION);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void stopDriver() {
        driver.quit();
    }

    @DataProvider(name = "positiveTestData")
    public Object[][] positiveTestData() {
        return new Object[][]{{("jasonbrown@test.com"), ("1111")}};
    }

    @DataProvider(name = "negativeTestData")
    public Object[][] negativeTestData() {
        return new Object[][]{{(""), ("")}};
    }

    @Test(dataProvider = "positiveTestData")
    public void signInPositiveTest(String userLogin, String userPassword) {
        driver.get(APPLICATION_URL);

        HeaderMenuComponent headerMenu = new HeaderMenuComponent(driver);
        HeaderMenuForLoggedInUserComponent loggedInUser = new HeaderMenuForLoggedInUserComponent(driver);
        SignInPageObject signInPage = new SignInPageObject(driver);

        headerMenu.clickOnSignInButton();
        signInPage.enterEmail(userLogin);
        signInPage.enterPassword(userPassword);
        signInPage.clickOnButtonSignIn();

        boolean actualResult = loggedInUser.visibilityOfUserIcon();
        final boolean EXPECTED_RESULT = true;

        Assert.assertEquals(actualResult, EXPECTED_RESULT);
    }

    @Test(dataProvider = "negativeTestData")
    public void signInNegativeTest(String userLogin, String userPassword) {
        driver.get(APPLICATION_URL);

        SignInPageObject signInPageObject = new SignInPageObject(driver);
        HeaderMenuComponent headerMenu = new HeaderMenuComponent(driver);
        SignInPageObject signInPage = new SignInPageObject(driver);

        headerMenu.clickOnSignInButton();
        signInPage.enterEmail(userLogin);
        signInPage.enterPassword(userPassword);
        signInPage.clickOnButtonSignIn();

        boolean actualResult = signInPageObject.isVisibleErrorEmailIsRequired() &&
                signInPageObject.isVisibleErrorPasswordIsRequired();
        final boolean EXPECTED_RESULT = true;

        Assert.assertEquals(actualResult, EXPECTED_RESULT);
    }
}